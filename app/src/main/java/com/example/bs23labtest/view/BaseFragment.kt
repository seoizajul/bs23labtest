package com.example.bs23labtest.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding

abstract class BaseFragment<T : ViewBinding> : Fragment(), FragmentInit {
    private var _binding: T? = null

    val binding: T get() = _binding!!

    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
       /* arguments?.let {
            this.onSetArgument(it)
        }*/
        super.onCreate(savedInstanceState)
        this.forFirstTime()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        navController = findNavController()
        _binding = setViewBind(inflater)
        this.initView()
        this.action()
        return binding.root
    }

    fun navigateTo(@IdRes actionId: Int, bundle: Bundle? = null) {
        if (this::navController.isInitialized) navController.safeNavigate(actionId, bundle)
    }

    fun navigateTo(direction:NavDirections) {
        if (this::navController.isInitialized) navController.navigate(direction)
    }

    fun popThis() = navController.popBackStack()

    abstract fun setViewBind(inflater: LayoutInflater): T

    private fun NavController.safeNavigate(@IdRes resId: Int, args: Bundle? = null) {
        currentDestination?.getAction(resId)?.run {
            navigate(resId, args)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}

interface FragmentInit {
    fun initView()
    fun action()
//    fun onSetArgument(arguments: Bundle){}
    fun forFirstTime(){}
}