package com.example.bs23labtest.view.listView

import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import com.example.bs23labtest.view.BaseFragment
import com.example.bs23labtest.databinding.FragmentListViewBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FragmentListView : BaseFragment<FragmentListViewBinding>() {

    private val mViewModel: ViewModelListView by viewModels()

    @Inject
    lateinit var adapter: ListViewAdapter

    override fun setViewBind(inflater: LayoutInflater): FragmentListViewBinding =
        FragmentListViewBinding.inflate(layoutInflater)

    override fun initView() {

        binding.apply {
            viewModel = mViewModel
            lifecycleOwner = viewLifecycleOwner
            listRC.adapter = adapter
        }

    }

    override fun action() {

        mViewModel.dataList.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }

        adapter.clickListener.clickListener = {
            navigateTo(FragmentListViewDirections.actionListViewToDetailsView(it))
            /*findNavController().navigate(FragmentListViewDirections.actionListViewToDetailsView(it.username))*/
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.listRC.adapter = null
    }

}