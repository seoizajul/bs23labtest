package com.example.bs23labtest.utils

import android.util.Log
import com.example.bs23labtest.BuildConfig
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DebugLogger @Inject constructor() {
    val tag: String = "Logger-Tag:"

    fun d(tag: String = this.tag, message: String) {
        logPrint(Log.DEBUG, message, tag)
    }

    fun i(tag: String = this.tag, message: String) {
        logPrint(Log.INFO, message, tag)
    }

    fun e(tag: String = this.tag, throwable: Throwable) {
        logPrint(Log.ERROR, throwable.toString(), tag)
    }

    fun e(tag: String = this.tag, message: String) {
        logPrint(Log.ERROR, message, tag)
    }

    private fun logPrint(priority: Int, print: String, _tag: String) {
        if (BuildConfig.DEBUG) Log.println(priority, _tag, print)
    }
}