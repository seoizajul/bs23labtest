package com.example.bs23labtest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : Application() {
    override fun onCreate() {
        if (BuildConfig.DEBUG) {
            // todo
        }
        super.onCreate()
    }
}