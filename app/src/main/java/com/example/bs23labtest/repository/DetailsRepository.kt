package com.example.bs23labtest.repository

import com.example.bs23labtest.network.domain.DetailsData
import com.example.bs23labtest.network.service.DetailsService
import com.example.bs23labtest.utils.DebugLogger
import javax.inject.Inject

class DetailsRepository @Inject constructor(
    val service: DetailsService,
    val logger: DebugLogger
) {
    suspend fun getDetails(name: String): DetailsData {
        return try {
            service.getUserDetails(name).detailsData
        } catch (e: Exception) {
            logger.e(throwable = e)
            DetailsData()
        }
    }
}