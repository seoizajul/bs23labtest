package com.example.bs23labtest.repository

import com.example.bs23labtest.network.domain.ListItemData
import com.example.bs23labtest.network.model.dataList
import com.example.bs23labtest.network.service.GetListService
import com.example.bs23labtest.utils.DebugLogger
import javax.inject.Inject

class ListRepository @Inject constructor(
    val service: GetListService,
    val logger: DebugLogger
) {

    suspend fun fetchUsers(): List<ListItemData> {
        try {
            return service.getRepoList().items.dataList()
        } catch (e: Exception) {
            logger.e(throwable = e)
        }
        return ArrayList()
    }
}