package com.example.bs23labtest.network.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
@Parcelize
data class DetailsData(
    val user: String? = "",
    val avatar: String? = "",
    val name: String? = "",
    val userSince: String? = "",
    val location: String? = ""
): Parcelable

