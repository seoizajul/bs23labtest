package com.example.bs23labtest.network.model

import com.example.bs23labtest.network.domain.ListItemData
import com.google.gson.annotations.SerializedName


data class ModelRepoResponse(
    @SerializedName("total_count") var totalCount: Int? = null,
    @SerializedName("incomplete_results") var incompleteResults: Boolean? = null,
    @SerializedName("items") var items: ArrayList<ModelListItem> = arrayListOf()
)

data class ModelListItem(
    @SerializedName("id") var id: Int? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("private") var private: Boolean? = null,
    @SerializedName("owner") var owner: ModelOwner? = ModelOwner(),
    @SerializedName("description") var description: String? = null,
    @SerializedName("updated_at") var updatedAt: String? = null,
    @SerializedName("visibility") var visibility: String? = null,
)

data class ModelOwner(
    @SerializedName("login") var login: String? = null,
    @SerializedName("id") var id: Int? = null,
    @SerializedName("avatar_url") var avatarUrl: String? = null,

)

fun List<ModelListItem>.dataList(): List<ListItemData> = map {
    ListItemData(
        id = it.id ?: -1,
        repoName = it.name ?: "",
        description = it.description ?: "",
        visibility = it.visibility ?: "",
        ownerName = it.owner?.login ?: "",
        ownerAvatar = it.owner?.avatarUrl ?: "",
        lastUpdate = it.updatedAt ?: ""
    )
}