package com.example.bs23labtest.network.service

import com.example.bs23labtest.network.model.ModelRepoResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface GetListService {
    @GET("/search/repositories")
    suspend fun getRepoList(
        @Query("q") query: String = "android" // by default android
    ): ModelRepoResponse
}