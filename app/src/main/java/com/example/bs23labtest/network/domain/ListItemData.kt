package com.example.bs23labtest.network.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ListItemData(
    val id: Int = 0,
    val repoName: String = "",
    val description:String = "",
    val visibility:String = "",
    val ownerName:String = "",
    val ownerAvatar:String = "",
    val lastUpdate: String = "",
): Parcelable
